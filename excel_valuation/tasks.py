import frappe

def excel_repost_entries():
    try:
        # Call the repost_entries function from the ERPNext module
        frappe.call('erpnext.stock.doctype.repost_item_valuation.repost_item_valuation.repost_entries')
        frappe.msgprint("Repost Entries Completed Successfully")
    except Exception as e:
        frappe.msgprint(f"Error: {str(e)}")
