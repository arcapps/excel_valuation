from frappe import _

def get_data():
	return [
		{
			"module_name": "Excel Valuation",
			"type": "module",
			"label": _("Excel Valuation")
		}
	]
